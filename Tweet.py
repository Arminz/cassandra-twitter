import re


class Tweet:
    def __init__(self, text, user_id = None):
        self.text = text
        self.hashtags = re.findall(r"#(\w+)", text)
        self.user_id = user_id
