import repository as repository
from Tweet import Tweet

while True:
    repository.init()
    command = input("command? 1-tweet 2-read all 3-read hashtags 4-delete tweet "
                    "5-delete user tweets 6-update tweets 7-read user tweets\n")
    print(command)
    if command == "1":
        user_id = int(input("user_id: "))
        tweet = Tweet(input("text: "), user_id)
        repository.insert_tweet(tweet)
    elif command == "2":
        rows = repository.read_all()
        for row in rows:
            print(row)
    elif command == "3":
        hashtag = input("hashtag: ")
        rows = repository.read_hashtag(hashtag)
        for row in rows:
            print(row)
    elif command == "4":
        user_id = int(input("user_id: "))
        tweet_id = input("tweet_id: ")
        repository.delete(tweet_id, user_id)
    elif command == "5":
        user_id = int(input("user_id: "))
        repository.delete_contains_user_id(user_id)
    elif command == "6":
        tweet_id = input("tweet_id: ")  # Don't user quotation
        user_id = int(input("user_id: "))
        tweet = Tweet(input("text: "))
        repository.update(tweet_id, user_id, tweet)
    elif command == "7":
        user_id = int(input("user_id: "))
        rows = repository.read_user_tweets(user_id)
        for row in rows:
            print(row)
