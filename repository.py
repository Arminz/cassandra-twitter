from uuid import UUID

from cassandra.cluster import Cluster

cluster = Cluster(['127.0.0.1'])
try:
    session = cluster.connect('test')
except Exception:
    session = cluster.connect()
    session.execute("create keyspace test  with replication={'class':'SimpleStrategy', 'replication_factor': 3};")
    session.execute("user test")


def init():
    query = 'CREATE TABLE IF NOT EXISTS tweets (user_id varint, tweet_id timeuuid, createdAt time, tweet text,hashtags set<text>, primary key (user_id, tweet_id));'
    session.execute(query)
    session.execute('CREATE INDEX IF NOT EXISTS ON tweets(hashtags);')
    session.execute('CREATE INDEX IF NOT EXISTS ON tweets(tweet_id);')
    # session.execute('CREATE INDEX IF NOT EXISTS ON tweets(user_id);')

def drop():
    session.execute('drop table tweets')


def insert_tweet(tweet):
    session.execute('insert into tweets (createdAt, user_id, tweet_id, tweet, hashtags) values (toUnixTimeStamp(now()),%s, now(), %s, %s);',
                    [tweet.user_id, tweet.text, set(tweet.hashtags)])


def read_all():
    rows = session.execute('select * from tweets LIMIT 10')
    return rows


def read_hashtag(hashtag):
    rows = session.execute("SELECT * FROM tweets WHERE hashtags CONTAINS %s LIMIT 10;", [hashtag])
    return rows


def read_user_tweets(user_id):
    rows = session.execute("SELECT * FROM tweets WHERE user_id = %s ORDER BY tweet_id DESC LIMIT 10;", [user_id])
    return rows


def update(tweet_id, user_id, tweet):
    session.execute("UPDATE tweets SET tweet = %s, hashtags = %s WHERE tweet_id = %s and user_id = %s",
                    [tweet.text, set(tweet.hashtags), UUID(tweet_id), user_id])


def delete(tweet_id, user_id):
    session.execute("DELETE FROM tweets WHERE tweet_id = %s and user_id = %s;", [UUID(tweet_id), user_id])


# def delete_contains_user_id(user_id):
#     rows = session.execute("SELECT * FROM tweets WHERE user_id = %s;", [user_id])
#     for row in rows:
#         tweet_id = row.tweet_id
#         session.execute("DELETE FROM tweets WHERE tweet_id = %s;", [tweet_id])


def delete_contains_user_id(user_id):
    session.execute("DELETE FROM tweets WHERE user_id = %s;", [user_id])
