import repository
import re


def add_tweet(tweet):
    tweet.hashtags = re.findall(r"#(\w+)", tweet.text)
    repository.insert_tweet(tweet)
